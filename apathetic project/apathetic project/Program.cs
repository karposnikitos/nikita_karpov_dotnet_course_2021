﻿using System;

namespace apathetic_project
{
    class Program
    {
        static void Main(string[] args)
        {
            int number1 = 8;
            int number2 = 228;
            int numberswap = number1;
            number1 = number2;
            number2 = numberswap;
            Console.WriteLine(number1 + " " + number2);
        }
    }
}
